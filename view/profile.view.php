<?php include('partials/_header.php');?>
<?php

$title=' Page de profile';  

?>
  <?php include('partials/error.php');?>

<section id="profile_container">
    <div class="profile"> 
        <h1 id="titre_profile">profile de <?= e($data->pseudo)?></h1>
     <!-- profile background   <div class="user_profile" style="<?= $data->src_user ? ' background-image: url(' .e($data->src_user).')':'background-color:blue'?>;background-size: cover;"> -->
      <div class="user_profile">
        
            <div class="div_image" style="<?= $data->src_user ? 'background-image: url( '.$data->src_user.')' : 'background-color:blue'?>;background-size: cover;">
               
            </div>
        
            <h1 class="nom"><?= e($data->name)?></h1>
            <span class="email">  <i  class="fas fa-id-card" id="icone"></i>&nbsp;<a href="mailto: <?= e($data->email)?>"><?= e($data->email)?></a></span>
           <br> <span class="pays"> <?= $data->pays ? '<i class="fa fa-location-arrow"></i>  '.e($data->pays).'-'.e($data->ville).'<br> <a href="https://www.google.com/maps?q='.e($data->pays).' '.e($data->ville).' " target="_blank">Voir dans carte </a> ' : ''?> </span>
           
        </div>
        <div class="job">
        <br> <span class="twitter">  <?= $data->twitter ? ' <a href="//twitter.com/'.e($data->twitter).' "> <i class="fab fa-twitter" id="icone"></i>  @'.e($data->twitter) . '</a>' : ''?> </span>
        <br> <span class="github"> <?= $data->github ? '  <a href="//github.com/'.e($data->github).' "> <i class="fab fa-github" id="icone"></i>  @'.e($data->github) . '</a>' : ''?> </span>
        <br> <span class="emploi"> <?= $data->sex=="F" ? '   <i class="fa fa-female" id="icone"></i> ': ' <i class="fa fa-male "id="icone"></i>'?> 
     
            <?php if($data->job_available==1){
                echo'disponible pour emploi ';
            }else{
                echo 'Non disponible pour emploi ';

            }?>
            </span>
       </div>
           
        
       
           
            
      
        
        <div class="description">
            <h3>Qui est <?= e($data->name)?> ?</h3>
            <p><?= e($data->bio)?></p>

        </div>
        <div class="amis">
                     <span>0</span>   <i>amis</i>
        </div>
        
    </div>
    
    <form action="" class="profile_complet" method="post" enctype="multipart/form-data">
    <div class="profile_complet_titre"><h1>Compléter mon profil</h1></div>
        <div class="profile_complet_input">
            <label  class="input1" for="nom">
               <span>nom <strong class="etoile">*</strong></span>
               <br> <input type="text"  name="nom" id="nom"class="champ" value="<?= get_input('nom') ? get_input('nom'):e($data->name)  ?>">
            </label> 
            <label class="input2"  for="ville">
               <span>ville <strong class="etoile">*</strong></span>
               <br> <input type="text"  name="ville" id="ville"class="champ" value="<?=get_input('ville') ? get_input('ville'):e($data->ville) ?>">
            </label>
            <label  class="input3" for="pays">
                <span>Pays <strong class="etoile">*</strong></span>
                <br><input type="text"  name="pays" id="pays"class="champ" value="<?=get_input('pays') ? get_input('pays'):e($data->pays)?>">
            </label>
            <label  class="input4" for="sexe">
                <span>sexe <strong class="etoile">*</strong></span>
                <br><select name="sexe"  id="sexe"class="champ" value="<?=get_input('sexe') ? get_input('sexe'):e($data->sexe)?>">
                    <option  value="H">homme</option>
                    <option <?php if(get_input('sexe')=='femme'){echo'selected';}?> value="F">femme</option>
                </select>
            </label> 
            <label class="input5"  for="twitter">
               <span>Twitter  <i class="fab fa-twitter" id="icone"></i> </span>
               <br> <input type="text"   name="twitter" id="Twitter"class="champ" value="<?=get_input('twitter') ? get_input('twitter'):e($data->twitter)?>">
            </label> 
            <label  class="input6" for="github">
                <span>Github  <i class="fab fa-github" id="icone"></i> </span>
                <br><input type="text"   name="github" id="Github"class="champ" value="<?=get_input('github') ? get_input('github'):e($data->github)?>">
            </label> 
            <label for="image">
                    Changer l'image de profile: <input type="file" name="image" id="image">
                </label>
            <label  class="input7" for="job_available">
                <input type="checkbox" name="job_available"   id="checkbox" > <span id="job_available">Disponible pour emploi?</span> 
            </label> 
            
            
              
            
            
        </div>
        <div class="profile_complet_biographie">
           <span id="biographie_titre">Biographie</span> <textarea name="bio" id="text_erea"  rows="10" ><?=get_input('bio') ? get_input('bio'):e($data->bio)?></textarea>
        </div>
        <input type="submit" value="valider"name='update'>
        
    

    </form>
        
    
           
            
        
 </section>
       
<?php include('partials/_footer.php'); ?>


    
    
 
