<!DOCTYPE html>
<html lang="en">
<head>
  
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php
      echo isset($title)
      ? $title.'-Boom Social Network' : 'Bomm Social Network rapide efficace et simple';
      ?>
  
    </title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="asstes/css/fontawesome-free-5.15.4-web/css/all.css" type="text/css"  rel="stylesheet"/>
    <link rel="stylesheet" href="assets/css/style.profile.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/register.css">
</head>
<body>
  <?php include('includes/constants.php') ;
  include('includes/fonctions.php');
  ?>
    <header>
      
      <?php 
      if(get_profile()){
        include('_nav.profile.php');
      }
      else{
        include('_nav.php');
      }
      
      
      ?>
      
      
    </header>
    <?php include('partials/_flash.php'); ?>
